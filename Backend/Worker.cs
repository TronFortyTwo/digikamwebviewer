﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace digiKamWebViewer
{
    class Worker : BackgroundService
    {
        public static ILogger<Worker> Logger;

        public Worker(ILogger<Worker> workerLogger)
        {
            Logger = workerLogger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Logger.LogInformation(string.Format("Started digiKamWebViewer v{0}", Assembly.GetEntryAssembly().GetName().Version.ToString()));

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
            }
        }
    }

}
