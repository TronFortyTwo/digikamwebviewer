
# digiKamWebView

API of digiKamWebViewer.
App repository: https://gitlab.com/AlessandroLucchet/digikamwebviewer

<!--- If we have only one group/collection, then no need for the "ungrouped" heading -->



## Endpoints

* [auth](#auth)
    1. [auth](#1-auth)
* [albums](#albums)
    1. [list](#1-list)
    1. [listimage](#2-listimage)
    1. [tree](#3-tree)
* [albumroots](#albumroots)
    1. [list](#1-list-1)
* [images](#images)
    1. [listalbum](#1-listalbum)
    1. [listids](#2-listids)
    1. [listtags](#3-listtags)
    1. [listdate](#4-listdate)
    1. [listwithposition](#5-listwithposition)
    1. [thumbnail](#6-thumbnail)
    1. [picture](#7-picture)
    1. [rawfile](#8-rawfile)
    1. [downloadalbum](#9-downloadalbum)
    1. [downloadids](#10-downloadids)
    1. [downloadtags](#11-downloadtags)
    1. [downloaddate](#12-downloaddate)
    1. [streamindex](#13-streamindex)
    1. [stream](#14-stream)
* [imagesdates](#imagesdates)
    1. [list](#1-list-2)
    1. [tree](#2-tree)
* [tags](#tags)
    1. [list](#1-list-3)
    1. [listimage](#2-listimage-1)
    1. [tree](#3-tree-1)
    1. [thumbnail](#4-thumbnail)
* [system](#system)
    1. [system/stats](#1-systemstats)
    1. [system/backendversion](#2-systembackendversion)
    1. [system/thumbnailswipe](#3-systemthumbnailswipe)
    1. [system/thumbnailsgeneratorstatus](#4-systemthumbnailsgeneratorstatus)
    1. [system/thumbnailsgeneratorstart](#5-systemthumbnailsgeneratorstart)
    1. [system/thumbnailsgeneratorstop](#6-systemthumbnailsgeneratorstop)

--------



## auth

Login with username and password



### 1. auth


Login with username and password


***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: {{APIURL}}/auth
```



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| username | {{USERNAME}} |  |
| password | {{PASSWORD}} |  |



## albums



### 1. list


Get a list of all albums or of a specific album


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/albums/list
```



### 2. listimage


Get a list of all albums or of a specific album


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/albums/listimage
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 34263 |  |



### 3. tree


Get a tree view of all albums


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/albums/tree
```



## albumroots



### 1. list


Get a list of all albumroots. An albumroot contains the identifier and the path of the contained images


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/albumroots/list
```



## images



### 1. listalbum


Get a list of images contained in a specific album


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/listalbum/
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 712 |  |



### 2. listids


Get a list of images with all specified ids


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/listids/
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| ids | 602,123 |  |



### 3. listtags


Get a list of images with all specified tags


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/listtags
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| ids | 230,33 |  |



### 4. listdate


Get a list of images with specific date


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/listdate
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| date | 202105 |  |



### 5. listwithposition


Get a list of all images with position set


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/listwithposition
```



### 6. thumbnail


Get the thumbnail of a specific image


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/thumbnail
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 35855 |  |



### 7. picture


Get a resized version of a specific image. If size is not specified, it will be used the default image size


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/picture
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 34259 |  |



### 8. rawfile


Get the raw file of a specific image


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/rawfile
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 17910 |  |



### 9. downloadalbum


Download an entire album in zip format


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/downloadalbum/
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 602 |  |



### 10. downloadids


Download a zip with images with all specified ids


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/downloadids/
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| ids | 602,123 |  |



### 11. downloadtags


Download a zip with images with all specified tags


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/downloadtags
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| ids | 33,44,42,48 |  |



### 12. downloaddate


Download a zip with images of a specific date


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/downloaddate
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| date | 200611 |  |



### 13. streamindex


Get the m3u8 index file of the video


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/streamindex
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 1352 |  |



### 14. stream


Get a TS segment of a video


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/images/stream
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| segment | 1303 |  |
| id | 22067 |  |



## imagesdates



### 1. list


Get a tree view of all imagesdates


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/imagedates/list
```



### 2. tree


Get a tree view of all imagesdates


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/imagedates/tree
```



## tags



### 1. list


Get a list of all tags


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/tags/list
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| ids | 230,33 |  |



### 2. listimage


Get the list of tags associated with a specific image


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/tags/listimage
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 28805 |  |



### 3. tree


Get a tree view of all tags


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/tags/tree
```



### 4. thumbnail


Get a thumbnail of a specific tag


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/tags/thumbnail
```



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| id | 583 |  |



## system



### 1. system/stats


Get statistics about pictures and videos


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/system/stats
```



### 2. system/backendversion


Get the version of the backend


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/system/backendversion
```



### 3. system/thumbnailswipe


Wipe all thumnbails from database


***Endpoint:***

```bash
Method: POST
Type: 
URL: {{APIURL}}/system/thumbnailswipe
```



### 4. system/thumbnailsgeneratorstatus


Batch create thumbnails for all images


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{APIURL}}/system/thumbnailsgeneratorstatus
```



### 5. system/thumbnailsgeneratorstart


Start the creation of thumnbails for all images in the database


***Endpoint:***

```bash
Method: POST
Type: 
URL: {{APIURL}}/system/thumbnailsgeneratorstart
```



### 6. system/thumbnailsgeneratorstop


Start the creation of thumnbails for all images in the database


***Endpoint:***

```bash
Method: POST
Type: 
URL: {{APIURL}}/system/thumbnailsgeneratorstop
```



---
[Back to top](#digikamwebview)

>Generated at 2023-09-21 21:56:40 by [docgen](https://github.com/thedevsaddam/docgen)
