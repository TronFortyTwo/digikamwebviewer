﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace digiKamWebViewer
{
    public class VolumeMapping
    {
		private static VolumeMapping instance;
		private Dictionary<String, String> mapping = null;

		public static VolumeMapping GetInstance()
		{
			if (instance == null)
				instance = new VolumeMapping();
			return instance;
		}

		/// <summary>
		/// Create a mapping between volume serial number and volume letter
		/// </summary>
		private VolumeMapping()
        {
			mapping = new Dictionary<string, string>();

			// If operating system is Windows
			if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				ManagementObjectSearcher ms = new ManagementObjectSearcher("Select * from Win32_LogicalDisk");
				foreach (ManagementObject mo in ms.Get())
				{
					if(mo["VolumeSerialNumber"]!=null)
						mapping.Add(mo["VolumeSerialNumber"].ToString().ToUpper(), mo["DeviceID"].ToString());
				}
			}else if(System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
				// To implement
            }
		}

		/// <summary>
		/// Get the drive letter of the passed volumeid
		/// </summary>
		/// <param name="uuid">The identifier stored in digiKam database</param>
		/// <returns></returns>
		public string getVolumePath(string uuid)
        {
			MatchCollection matches = Regex.Matches(uuid, "volumeid:\\?uuid=([a-fA-F0-9]{8})");
			foreach (Match match in matches)
				return mapping[match.Groups[1].Value.ToUpper()];
			return null;
        }
    }
}
