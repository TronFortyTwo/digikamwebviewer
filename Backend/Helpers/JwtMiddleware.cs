using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace digiKamWebViewer.Helpers
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings appSettings;

        public JwtMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next;
            this.appSettings = appSettings.Value;
        }

        public static String GetToken(HttpRequest request)
        {
            // Search for token in the request header
            string token = request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            // If no token found, search it in the request params
            if (token == null)
                token = request.Query["Token"];

            return token;
        }

        public async Task Invoke(HttpContext context, IUserProvider userService)
        {
            // Get the token
            string token = GetToken(context.Request);

            // If a token is found, attach to the request the context needed to be authorized
            if (token != null)
            {
                try
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(appSettings.Secret);
                    tokenHandler.ValidateToken(token, new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                        ClockSkew = TimeSpan.Zero
                    }, out SecurityToken validatedToken);

                    var jwtToken = (JwtSecurityToken)validatedToken;
                    var username = jwtToken.Claims.First(x => x.Type == "username").Value;

                    // attach user to context on successful jwt validation
                    context.Items["User"] = userService.GetByUsername(username);
                }
                catch
                {
                    // do nothing if jwt validation fails
                    // user is not attached to context so request won't have access to secure routes
                }
            }

            await _next(context);
        }
    }
}