﻿using System;
using System.Collections.Generic;
using System.IO;

namespace digiKamWebViewer.Helpers
{
    public class DatabaseDetector
    {
        /// <summary>
        /// Search for digikam database path opening all digikamrc files found on users profile directories
        /// </summary>
        /// <returns></returns>
        public static List<String> GetDigikamDatabasePaths()
        {
            // List user profile directories
            DirectoryInfo[] userProfileDirectories = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)).Parent.GetDirectories();

            // Iterate over profile directories
            List<String> databasePaths = new List<string>();
            foreach (DirectoryInfo userProfileDirectory in userProfileDirectories)
            {
                // Digikam settings file
                string digikamrc = Path.Combine(userProfileDirectory.FullName, "AppData", "Local", "digikamrc");
                if (File.Exists(digikamrc))
                {
                    // If exists, read content into a dictionary
                    var lines = File.ReadAllLines(digikamrc);
                    var dict = new Dictionary<string, string>();
                    foreach (var s in lines)
                    {
                        var split = s.Split("=");
                        if (split.Length == 2)
                            dict.TryAdd(split[0], split[1]);
                    }

                    // Get the database path
                    if (dict["Database Type"] == "QSQLITE")
                        databasePaths.Add(Path.Combine(dict["Database Name"]));
                }
            }

            return databasePaths;
        }
    }
}
