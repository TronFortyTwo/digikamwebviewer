﻿using ImageMagick;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace digiKamWebViewer.Helpers
{
    public class ThumbnailsFactory
    {
        /// <summary>
        /// Create a resized version of the passed picture
        /// </summary>
        /// <param name="filePath">Path of the picture</param>
        /// <param name="minSize">Minimum size of width and height that the resized picture should have</param>
        /// <returns></returns>
        public static MemoryStream CreateFromPicture(string filePath, int minSize)
        {
            return CreateFromPicture(filePath, minSize, Rectangle.Empty);
        }

        /// <summary>
        /// Create a resized version of the passed picture
        /// </summary>
        /// <param name="filePath">Path of the picture</param>
        /// <param name="minSize">Minimum size of width and height that the resized picture should have</param>
        /// <param name="rectangle">Use Rectangle.Empty if you want to resize the whole picture. Put a rectangle if you want to use only a portion of the original picture</param>
        /// <returns></returns>
        public static MemoryStream CreateFromPicture(string filePath, int minSize, Rectangle rectangle)
        {
            return CreateFromPicture(ReadBitmap(filePath), minSize, rectangle);
        }
        public static MemoryStream CreateFromPicture(Bitmap fullBitmap, int minSize, Rectangle rectangle)
        {
            // If necessary, extract the rectangle portion of the image
            Bitmap toResize;
            if (rectangle.IsEmpty)
                toResize = fullBitmap;
            else
            {
                //bitmap = imagebitmap.Clone(rectangle, System.Drawing.Imaging.PixelFormat.DontCare);
                toResize = new Bitmap(rectangle.Width, rectangle.Height);
                using (var gr = Graphics.FromImage(toResize))
                    gr.DrawImage(fullBitmap, new Rectangle(0, 0, toResize.Width, toResize.Height), rectangle, GraphicsUnit.Pixel);
            }

            // Resize to minSize
            Bitmap resized;
            if (toResize.Width >= toResize.Height)
                resized = new Bitmap(toResize, new Size(
                    toResize.Width * Math.Min(toResize.Height, minSize) / toResize.Height, 
                    Math.Min(toResize.Height, minSize)
                    ));
            else
                resized = new Bitmap(toResize, new Size(
                    Math.Min(toResize.Width, minSize),
                    toResize.Height * Math.Min(toResize.Width, minSize) / toResize.Width
                    ));

            // Convert to JPG stream
            var stream = new System.IO.MemoryStream();
            resized.Save(stream, ImageFormat.Jpeg);
            stream.Position = 0;

            // Dispose memory
            fullBitmap.Dispose();
            toResize.Dispose();
            resized.Dispose();

            return stream;
        }

        /// <summary>
        /// Create a thumbnail for a video
        /// </summary>
        /// <param name="filePath">Path of the video</param>
        /// <param name="minSize">Minimum size of width and height that the resized picture should have</param>
        /// <returns></returns>
        public static async Task<MemoryStream> CreateFromVideoAsync(string filePath, int minSize)
        {
            // Start FFmpeg in order to create the thumbnail of the video
            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-i \"{filePath}\" -vf \"select =eq(n\\,0),scale='if(gte(ih\\,iw)\\,{minSize}\\,-1)':'if(gte(ih\\,iw)\\,-1,{minSize})'\" -vframes 1 -f image2pipe -",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                StandardOutputEncoding = Encoding.UTF8
            };

            // Create the memory stream to get the output of ffmpeg
            var memoryStream = new MemoryStream();
            using (Process process = new Process())
            {
                process.StartInfo = processStartInfo;
                process.Start();
                await process.StandardOutput.BaseStream.CopyToAsync(memoryStream);
                process.WaitForExit();
                memoryStream.Seek(0, SeekOrigin.Begin);
            }

            return memoryStream;
        }

        /// <summary>
        /// Create a preview for an audio file creating a Waves Pic with ffmpeg
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="minSize"></param>
        /// <returns></returns>
        public static async Task<MemoryStream> CreateFromAudioAsync(string filePath, int minSize)
        {
            // Start FFmpeg in order to create the thumbnail of the video
            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-i \"{filePath}\" -filter_complex \"aformat=channel_layouts=mono,showwavespic=s={minSize*18/9}x{minSize}:colors=blue\" -vframes 1 -f image2pipe -",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                StandardOutputEncoding = Encoding.UTF8
            };

            // Create the memory stream to get the output of ffmpeg
            var memoryStream = new MemoryStream();
            using (Process process = new Process())
            {
                process.StartInfo = processStartInfo;
                process.Start();
                await process.StandardOutput.BaseStream.CopyToAsync(memoryStream);
                process.WaitForExit();
                memoryStream.Seek(0, SeekOrigin.Begin);
            }

            return memoryStream;
        }

        /// <summary>
        /// Read a file and create a bitmap
        /// </summary>
        /// <param name="filePath">Path of the picture</param>
        /// <returns></returns>
        public static Bitmap ReadBitmap(string filePath)
        {
            Bitmap fullBitmap;

            // Load heic format with Magick.NET library
            if (Path.GetExtension(filePath).ToLower() == ".heic")
            {
                using (MemoryStream memStream = new MemoryStream())
                {
                    using (MagickImage image = new MagickImage(filePath))
                    {
                        // Sets the output format to jpg
                        image.Format = MagickFormat.Jpeg;

                        // Write the image to the memorystream
                        image.Write(memStream);
                        fullBitmap = new Bitmap(memStream);
                    }
                }
            }
            else
            {
                // Open image file
                System.Drawing.Image imagefile = System.Drawing.Image.FromFile(filePath);
                fullBitmap = new Bitmap(imagefile);

                // Rotate thumbnail according to EXIF data
                try
                {
                    switch (BitConverter.ToInt16(imagefile.GetPropertyItem(0x0112).Value, 0))
                    {
                        case 3:
                            fullBitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            break;
                        case 6:
                            fullBitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            break;
                        case 8:
                            fullBitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            break;
                    }
                }
                catch (Exception e)
                {
                    // No EXIF property available
                    //Console.WriteLine($"No EXIF property available for picture {filePath}: {e.Message}");
                }

                // Dispose memory
                imagefile.Dispose();
            }
            return fullBitmap;
        }
    }
}
