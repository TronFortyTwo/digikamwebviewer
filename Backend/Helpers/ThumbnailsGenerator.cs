﻿using digiKamWebViewer.Models;
using digiKamWebViewer.Repository;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace digiKamWebViewer
{
    public class ThumbnailsGenerator
    {
		private static ThumbnailsGenerator instance;
		private ThumbnailsGeneratorStatus status;
        private Task task;
        private CancellationTokenSource cancellationToken;

        public static ThumbnailsGenerator GetInstance()
		{
			if (instance == null)
				instance = new ThumbnailsGenerator();
			return instance;
		}

		private ThumbnailsGenerator()
        {
            status = new ThumbnailsGeneratorStatus();
        }

        /// <summary>
        /// Get the status of the worker
        /// </summary>
        /// <returns></returns>
        public ThumbnailsGeneratorStatus GetStatus()
        {
            return status;
        }

        /// <summary>
        /// Start the bulk creation of thumbnails
        /// </summary>
        /// <param name="tagProvider"></param>
        /// <param name="imageProvider"></param>
        /// <param name="thumbnailProvider"></param>
        public void Start(ITagProvider tagProvider, IImageProvider imageProvider, IThumbnailProvider thumbnailProvider)
        {
            Start(tagProvider,imageProvider,thumbnailProvider,new Progress<ThumbnailsGeneratorStatus>(currentStatus => status = currentStatus));
        }

        private void Start(ITagProvider tagProvider, IImageProvider imageProvider, IThumbnailProvider thumbnailProvider, IProgress<ThumbnailsGeneratorStatus> progress)
        {
            if (task == null || task.IsCompleted || task.IsCanceled || task.IsFaulted)
            {
                cancellationToken = new CancellationTokenSource();
                task = Task.Run<ThumbnailsGeneratorStatus>(async () =>
                {
                    // Init
                    List<Tag> tags = (List<Tag>)tagProvider.Get().Result;
                    List<Image> images = (List<Image>)imageProvider.Get().Result;
                    status.Start(tags.Count + images.Count);

                    // Generate task queue
                    List<Func<Task>> taskQueue = new List<Func<Task>>();

                    // Generate for tags
                    foreach (var tag in tags)
                    {
                        taskQueue.Add(async () =>
                        {
                            await thumbnailProvider.GetByTag(tag.id);
                        });
                    }

                    // Generate for images
                    foreach (var image in images)
                    {
                        taskQueue.Add(async () =>
                        {
                            await thumbnailProvider.GetByImage(image.id);
                        });
                    }

                    Parallel.ForEach(taskQueue, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, action =>
                    {
                        action.Invoke();

                        // Update status
                        status.IncrementCounter();
                        progress.Report(status);

                        // Check for stop
                        if (cancellationToken.Token.IsCancellationRequested)
                            cancellationToken.Token.ThrowIfCancellationRequested();
                    });

                    status.isRunning = false;

                    return status;
                }, cancellationToken.Token);
            }
        }

        /// <summary>
        /// Stop the bulk creation of thumbnails
        /// </summary>
        public void Stop()
        {
            if (task != null)
            {
                cancellationToken.Cancel();
                status.Stop();
            }
        }
    }
}
