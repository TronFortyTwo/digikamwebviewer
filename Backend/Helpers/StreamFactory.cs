﻿using digiKamWebViewer.Models;
using System.Threading.Tasks;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace digiKamWebViewer.Helpers
{
    public class StreamFactory
    {
        private static string VideoCodec = "libx264";
        private static string AudioCodec = "aac";
        private static string OutputFormat = "mpegts";
        private static string ScaleOption = "scale=-2:min'(720,ih)'";

        /// <summary>
        /// Generate the m3u8 index file
        /// </summary>
        /// <param name="video">Video object to analyze</param>
        /// <param name="VideoSegmentDuration">Segment max duration</param>
        /// <param name="token">The token to add to the segments for authentication</param>
        /// <returns>The string content of the m3u8</returns>
        public static async Task<string> GetStreamIndex(Image video, int VideoSegmentDuration, string token)
        {
            // Get video metadata
            double VideoDuration = GetMediaDuration(video.GetFilePath());
            int SegmentCount = (int)Math.Ceiling(VideoDuration / VideoSegmentDuration);

            // Setup m3u8 header
            string m3u8Content =
                "#EXTM3U\n" +
                "#EXT-X-VERSION:3\n" +
                $"#EXT-X-TARGETDURATION:{VideoSegmentDuration}\n" +
                "#EXT-X-MEDIA-SEQUENCE:0\n";

            // Create segments rows
            for (int segment = 0; segment < SegmentCount; segment++)
            {
                int ChunkDuration = (int)(segment < SegmentCount - 1 ? VideoSegmentDuration : VideoDuration % VideoSegmentDuration);
                m3u8Content += $"#EXTINF:{ChunkDuration},\n";
                m3u8Content += $"Stream?id={video.id}&segment={segment:D6}&token={token}\n";
            }

            m3u8Content += "#EXT-X-ENDLIST\n";
            return m3u8Content;
        }

        /// <summary>
        /// Generate the TS segment of the video
        /// </summary>
        /// <param name="video">Video object to analyze</param>
        /// <param name="segment">Segment number</param>
        /// <param name="VideoSegmentDuration">Segment max duration</param>
        /// <returns>The MemoryStream of the TS file</returns>
        public static async Task<MemoryStream> GetStreamSegment(Image video, int segment, int VideoSegmentDuration)
        {
            // Start FFmpeg in order to create the TS file
            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-ss {segment * VideoSegmentDuration} -i \"{video.GetFilePath()}\" -t {VideoSegmentDuration} -c:v {VideoCodec} -vf \"{ScaleOption}\" -c:a {AudioCodec} -f {OutputFormat} -output_ts_offset {segment * VideoSegmentDuration} -",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                StandardOutputEncoding = Encoding.UTF8
            };

            // Create the memory stream to get the output of ffmpeg
            var memoryStream = new MemoryStream();
            using (Process process = new Process())
            {
                process.StartInfo = processStartInfo;
                process.Start();
                await process.StandardOutput.BaseStream.CopyToAsync(memoryStream);
                process.WaitForExit();
                memoryStream.Seek(0, SeekOrigin.Begin);
            }

            return memoryStream;
        }


        /// <summary>
        /// Get the duration of a media
        /// </summary>
        /// <param name="mediaPath">Media path</param>
        /// <returns>Duration (in seconds) of the media</returns>
        private static double GetMediaDuration(string mediaPath)
        {
            // Start FFmpeg in order to create the TS file
            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-i \"{mediaPath}\"",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                StandardOutputEncoding = Encoding.UTF8
            };

            // Get the output of ffmpeg
            Process process = new Process();
            process.StartInfo = processStartInfo;
            process.Start();
            string ffmpegOutput = process.StandardError.ReadToEnd();
            process.WaitForExit();

            // Get the media duration
            try
            {
                string pattern = @"Duration: (\d\d:\d\d:\d\d.\d\d)";
                Match match = Regex.Match(ffmpegOutput, pattern);
                if (match.Success)
                {
                    TimeSpan duration = TimeSpan.ParseExact(match.Groups[1].Value, "hh\\:mm\\:ss\\.ff", CultureInfo.InvariantCulture);
                    return duration.TotalSeconds;
                }
            }catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }
    }
}
