﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using digiKamWebViewer.Models;
using digiKamWebViewer.Repository;

namespace digiKamWebViewer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class AlbumsController : ControllerBase
    {
        private readonly IAlbumProvider provider;

        public AlbumsController(IAlbumProvider provider)
        {
            this.provider = provider;
        }

        public async Task<IEnumerable<Album>> List(long id)
        {
            if (id == 0)
                return await provider.GetAll();
            else
                return await provider.GetById(id);
        }

        public async Task<IEnumerable<Album>> Tree()
        {
            return await provider.GetAllTree();
        }

        public async Task<IEnumerable<Album>> ListImage(long id)
        {
            return await provider.GetByImage(id);
        }
    }
}