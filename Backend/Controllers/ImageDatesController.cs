﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using digiKamWebViewer.Models;
using digiKamWebViewer.Repository;

namespace digiKamWebViewer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class ImageDatesController : ControllerBase
    {
        private readonly IImageDateProvider provider;

        public ImageDatesController(IImageDateProvider provider)
        {
            this.provider = provider;
        }

        public async Task<IEnumerable<ImageDate>> List()
        {
            return await provider.GetAll();
        }

        public async Task<IEnumerable<ImageDate>> Tree()
        {
            return await provider.GetAllTree();
        }
    }
}