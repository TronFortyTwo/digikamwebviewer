﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using digiKamWebViewer.Models;
using digiKamWebViewer.Repository;

namespace digiKamWebViewer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class AlbumRootsController : ControllerBase
    {
        private readonly IAlbumRootProvider provider;

        public AlbumRootsController(IAlbumRootProvider provider)
        {
            this.provider = provider;
        }

        [HttpGet]
        public async Task<IEnumerable<AlbumRoot>> List()
        {
            return await provider.GetAll();
        }
    }
}