﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using digiKamWebViewer.Models;
using digiKamWebViewer.Repository;

namespace digiKamWebViewer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class TagsController : ControllerBase
    {

        private readonly ITagProvider tagProvider;
        private readonly IThumbnailProvider thumbnailProvider;

        public TagsController(IThumbnailProvider thumbnailProvider, ITagProvider tagProvider)
        {
            this.tagProvider = tagProvider;
            this.thumbnailProvider = thumbnailProvider;
        }

        public async Task<IEnumerable<Tag>> List(string ids)
        {
            if(ids==null) return await tagProvider.Get();
            try
            {
                List<int> idsList = ids.Split(',').Select(Int32.Parse).ToList();
                return await tagProvider.GetByIds(ids);
            }
            catch(Exception e)
            {
                return await tagProvider.Get();
            }
        }

        public async Task<IEnumerable<Tag>> ListImage(long id)
        {
            return await tagProvider.GetByImage(id);
        }

        public async Task<IEnumerable<Tag>> Tree()
        {
            return await tagProvider.GetAllTree();
        }

        public async Task<IActionResult> Thumbnail(long id)
        {
            // Get the thumbnail
            Thumbnail tn = await thumbnailProvider.GetByTag(id);

            // Return
            if (tn != null)
                return File(tn.data, "image/jpeg");
            else
                return NotFound();
        }
    }
}