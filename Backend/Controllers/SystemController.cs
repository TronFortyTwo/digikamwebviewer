﻿using digiKamWebViewer.Models;
using digiKamWebViewer.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace digiKamWebViewer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class SystemController : ControllerBase
    {
        private readonly IThumbnailProvider thumbnailProvider;
        private readonly ITagProvider tagProvider;
        private readonly IImageProvider imageProvider;
        private readonly IStatsProvider statsProvider;
        
        public SystemController(IThumbnailProvider thumbnailProvider, ITagProvider tagProvider, IImageProvider imageProvider, IStatsProvider statsProvider)
        {
            this.thumbnailProvider = thumbnailProvider;
            this.tagProvider = tagProvider;
            this.imageProvider = imageProvider;
            this.statsProvider = statsProvider;
        }

        [HttpGet]
        public async Task<Dictionary<string, dynamic>> Stats()
        {
            Dictionary<string, dynamic> dictionary = new Dictionary<string, dynamic>();
            dictionary.Add("ImagesStat", (await statsProvider.GetImagesStats()));
            dictionary.Add("Counts", new
            {
                Albums = await statsProvider.GetAlbumsCount(),
                Tags = await statsProvider.GetTagsCount(),
                Images = await statsProvider.GetImagesCount(),
                Thumbnails = await statsProvider.GetThumbnailsCount()
            });
            return dictionary;
        }
        
        [HttpPost]
        public IActionResult ThumbnailsWipe()
        {
            thumbnailProvider.ClearThumbnails();
            return Ok();
        }

        [HttpPost]
        public void ThumbnailsGeneratorStart()
        {
            ThumbnailsGenerator.GetInstance().Start(tagProvider, imageProvider, thumbnailProvider);
        }

        [HttpPost]
        public void ThumbnailsGeneratorStop()
        {
            ThumbnailsGenerator.GetInstance().Stop();
        }

        [HttpGet]
        public ThumbnailsGeneratorStatus ThumbnailsGeneratorStatus()
        {
            //Dictionary<string, dynamic> dictionary = new Dictionary<string, dynamic>();
            //dictionary.Add("Status", );
            return ThumbnailsGenerator.GetInstance().GetStatus();
        }

        [HttpGet]
        public Dictionary<string, dynamic> BackendVersion()
        {
            Dictionary<string, dynamic> dictionary = new Dictionary<string, dynamic>();
            dictionary.Add("Version", Assembly.GetEntryAssembly().GetName().Version.ToString());
            return dictionary;
        }
    }
}