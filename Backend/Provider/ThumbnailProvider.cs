﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using digiKamWebViewer.Helpers;
using digiKamWebViewer.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace digiKamWebViewer.Repository
{
    public interface IThumbnailProvider
    {
        Task<Thumbnail> GetByImage(long imageid);
        Task<Thumbnail> GetByTag(long tagid);
        Task ClearThumbnails();
    }

    public class ThumbnailProvider : IThumbnailProvider
    {
        private readonly AppSettings appSettings;
        private readonly IImageProvider imageProvider;
        private readonly ITagProvider tagProvider;
        private readonly ILogger<ThumbnailProvider> logger;

        public ThumbnailProvider(IOptions<AppSettings> appSettings, IImageProvider imageProvider, ITagProvider tagProvider, ILogger<ThumbnailProvider> logger)
        {
            this.appSettings = appSettings.Value;
            this.imageProvider = imageProvider;
            this.tagProvider = tagProvider;
            this.logger = logger;
        }

        protected SqliteConnection GetConnection()
        {
            return new SqliteConnection(appSettings.GetThumbnailDatabaseConnectionString());
        }

        /// <summary>
        /// Return the thumnbail of a specified image
        /// </summary>
        /// <param name="imageid">The image id</param>
        /// <returns></returns>
        public async Task<Thumbnail> GetByImage(long imageid)
        {
            using (var connection = GetConnection())
            {
                // Check for cached thumbnail
                IEnumerator<digiKamWebViewer.Models.Thumbnail> cachedThumbnail = (await connection.QueryAsync<Thumbnail>("select imageid, modificationDate, data from Thumbnails where imageid = @id", new { id = imageid})).GetEnumerator();
                if (cachedThumbnail.MoveNext())
                    return cachedThumbnail.Current;

                // Get the image
                digiKamWebViewer.Models.Image image = await imageProvider.GetById(imageid);

                // Generate the thumnbail
                try
                {
                    // Generate a thumbnail with the parametrized size
                    MemoryStream stream = null;
                    if (image.IsPicture())
                        stream = ThumbnailsFactory.CreateFromPicture(image.GetFilePath(), appSettings.ThumbnailMinimumSize, Rectangle.Empty);
                    else if (image.IsVideo())
                    {
                        try
                        {
                            stream = await ThumbnailsFactory.CreateFromVideoAsync(image.GetFilePath(), appSettings.ThumbnailMinimumSize);
                        }
                        catch (System.AggregateException fnf)
                        {
                            logger.LogWarning($"Error while creating thumbnail for {image.GetFilePath()}: {fnf.InnerException.Message}");
                            return null;
                        }
                    }else if(image.IsAudio())
                    {
                        try
                        {
                            stream = await ThumbnailsFactory.CreateFromAudioAsync(image.GetFilePath(), appSettings.ThumbnailMinimumSize);
                        }
                        catch (System.AggregateException fnf)
                        {
                            logger.LogWarning($"Error while creating thumbnail for {image.GetFilePath()}: {fnf.InnerException.Message}");
                            return null;
                        }
                    }

                    // Save the thumbnail into the database
                    Thumbnail thumbnail = new Thumbnail();
                    if (stream != null)
                    {
                        thumbnail.imageid = imageid;
                        thumbnail.modificationDate = image.creationDate;
                        thumbnail.data = stream.ToArray();
                        await Set(thumbnail);
                    }
                    return thumbnail;
                }
                catch (OutOfMemoryException e)
                {
                    logger.LogWarning($"Error while creating and saving thumnbail for {image.GetFilePath()}: {e.Message}");
                    return null;
                }
            }
        }

        /// <summary>
        /// Get a thumbnail of a specified tag
        /// </summary>
        /// <param name="tagid">The tag id</param>
        /// <returns></returns>
        public async Task<Thumbnail> GetByTag(long tagid)
        {
            using (var connection = GetConnection())
            {
                // If thumbnail is cached, return it
                IEnumerator<digiKamWebViewer.Models.Thumbnail> cachedThumbnail = (await connection.QueryAsync<Thumbnail>("select imageid, modificationDate, data from Thumbnails where tagid = @id", new { id = tagid})).GetEnumerator();
                if (cachedThumbnail.MoveNext())
                    return cachedThumbnail.Current;

                // Get tagRect and image
                TagThumbnailProperty property = await tagProvider.GetThumbnailProperty(tagid);
                if (property != null)
                {
                    // Get the image
                    digiKamWebViewer.Models.Image image = await imageProvider.GetById(property.imageid);
                    if (image == null)
                        return null;

                    // Get the rectangle
                    Rectangle rectangle = Rectangle.Empty;
                    if (property.rect != null)
                    {
                        string pattern = "x=\"(?<x>\\d+)\"\\s+y=\"(?<y>\\d+)\"\\s+width=\"(?<width>\\d+)\"\\s+height=\"(?<height>\\d+)\"";
                        string input = property.rect;
                        Regex rgx = new Regex(pattern);
                        Match match = rgx.Match(input);
                        if (match.Success)
                        {
                            // Try to create a rectangle from parsed values
                            try
                            {
                                rectangle = new Rectangle(Int32.Parse(match.Groups["x"].Value), Int32.Parse(match.Groups["y"].Value), Int32.Parse(match.Groups["width"].Value), Int32.Parse(match.Groups["height"].Value));
                            }
                            finally { }
                        }
                    }

                    try
                    {
                        // Generate the thumnbail
                        MemoryStream stream = ThumbnailsFactory.CreateFromPicture(image.GetFilePath(), appSettings.ThumbnailMinimumSize, rectangle);

                        // Save the thumbnail into the database
                        Thumbnail thumbnail = new Thumbnail();
                        thumbnail.tagid = tagid;
                        thumbnail.modificationDate = image.creationDate;
                        thumbnail.data = stream.ToArray();
                        await Set(thumbnail);

                        return thumbnail;
                    }
                    catch (OutOfMemoryException e)
                    {
                        logger.LogWarning($"Error while creating and saving thumnbail for {image.GetFilePath()}: {e.Message}");
                        return null;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Save the thumbnail into the database
        /// </summary>
        /// <param name="thumbnail"></param>
        /// <returns></returns>
        public async Task Set(Thumbnail thumbnail)
        {
            using var connection = GetConnection();
            await connection.ExecuteAsync("INSERT OR REPLACE INTO Thumbnails (imageid, tagid, modificationDate, data) VALUES (@imageid, @tagid, @modificationDate, @data);", thumbnail);
        }

        /// <summary>
        /// Clear all stored thumbnail from the database
        /// </summary>
        /// <returns></returns>
        public async Task ClearThumbnails()
        {
            using var connection = GetConnection();
            await connection.ExecuteAsync("DELETE FROM Thumbnails");
        }
    }
}
