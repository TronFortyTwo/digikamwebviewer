﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using digiKamWebViewer.Helpers;
using digiKamWebViewer.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace digiKamWebViewer.Repository
{
    public interface IAlbumProvider
    {
        Task<IEnumerable<Album>> GetAll();
        Task<IEnumerable<Album>> GetById(long id);
        Task<IEnumerable<Album>> GetAllTree();
        Task<IEnumerable<Album>> GetByImage(long id);
    }

    public class AlbumProvider : IAlbumProvider
    {

        private readonly AppSettings appSettings;
        private readonly IAlbumRootProvider albumRootProvider;
        private string baseQuery = "select Albums.id, Albums.albumRoot, Albums.relativePath from Albums";

        public AlbumProvider(IOptions<AppSettings> appSettings, IAlbumRootProvider albumRootProvider)
        {
            this.appSettings = appSettings.Value;
            this.albumRootProvider = albumRootProvider;
        }

        /// <summary>
        /// Return all albums
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Album>> GetAll()
        {
            return GetWithMetadata(baseQuery + " where Albums.albumRoot > 0");
        }

        /// <summary>
        /// Return a specific album
        /// </summary>
        /// <param name="id">The id of the album</param>
        /// <returns></returns>
        public Task<IEnumerable<Album>> GetById(long id)
        {
            return GetWithMetadata(baseQuery + $" where Albums.id={id}");
        }
        public async Task<IEnumerable<Album>> GetWithMetadata(string query)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                IEnumerable<Album> albums = (await connection.QueryAsync<Album>(query));
                foreach (Album a in albums)
                {
                    // Setup album name
                    a.name = a.relativePath.Substring(a.relativePath.LastIndexOf('/') + 1);
                    if (a.name == "")
                    {
                        // If it's a root album, get the name of the AlbumRoot
                        AlbumRoot ar = albumRootProvider.GetById(a.albumRoot).Result;
                        a.name = ar.specificPath.Substring(ar.specificPath.LastIndexOf('/') + 1);
                    }

                    // Setup parent album
                    string parentRelativePath = a.relativePath.Substring(0, a.relativePath.LastIndexOf('/'));
                    if (parentRelativePath == "")
                        parentRelativePath = "/";
                    if (parentRelativePath != null && parentRelativePath != "")
                    {
                        IEnumerable<Album> p = albums.Where(al => al.relativePath.Equals(parentRelativePath) && al.albumRoot == a.albumRoot);
                        if (p.Count() > 0 && p.First().id != a.id)
                            a.pid = p.First().id;
                    }
                }
                return albums;
            }
        }

        /// <summary>
        /// Return a tree of all albums
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Album>> GetAllTree()
        {
            // Get flat List
            IEnumerable<Album> flatList = await GetAll();

            // Setup a new list with root albums
            List<Album> treeList = new List<Album>();
            treeList.AddRange(flatList.Where(album => album.pid == 0));

            // Setup parent-child relations
            foreach (Album parentEntry in flatList)
            {
                parentEntry.children = new List<Album>();
                parentEntry.children.AddRange(flatList.Where(album => album.pid == parentEntry.id));
            }

            return treeList;
        }

        /// <summary>
        /// Return the album associated with a specific image
        /// </summary>
        /// <param name="id">image id</param>
        /// <returns></returns>
        public Task<IEnumerable<Album>> GetByImage(long id)
        {
            return GetWithMetadata(baseQuery + $" inner join Images on (Images.album = Albums.id) where Images.id = {id}");
        }
    }
}
