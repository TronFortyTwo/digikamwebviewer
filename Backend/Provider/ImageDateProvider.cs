﻿using digiKamWebViewer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using digiKamWebViewer.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.Data.Sqlite;

namespace digiKamWebViewer.Repository
{
    public interface IImageDateProvider
    {
        Task<IEnumerable<ImageDate>> GetAll();
        Task<IEnumerable<ImageDate>> GetAllTree();
    }

    public class ImageDateProvider: IImageDateProvider
    {
        private readonly AppSettings appSettings;

        public ImageDateProvider(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Return all imagedate
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ImageDate>> GetAll()
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                IEnumerable<ImageDate> months = (await connection.QueryAsync<ImageDate>("select distinct STRFTIME('%Y',IFNULL(ImageInformation.digitizationDate,IFNULL(ImageInformation.creationDate,Images.modificationDate))) as year, STRFTIME('%m',IFNULL(ImageInformation.digitizationDate,IFNULL(ImageInformation.creationDate,Images.modificationDate))) as month " +
            "from Images " +
            "inner join ImageInformation on (Images.id = ImageInformation.imageid) " +
            "order by 1,2"));
                List<long> years = new List<long>();
                List<ImageDate> dates = new List<ImageDate>();

                foreach (ImageDate month in months)
                {
                    // Setup date id
                    month.id = Int32.Parse($"{month.year}{month.month:00}");
                    month.pid = month.year;
                    dates.Add(month);

                    // Create year, if not existing
                    if (!years.Contains(month.year))
                    {
                        ImageDate year = new ImageDate();
                        year.id = month.year;
                        year.year = month.year;
                        years.Add(month.year);
                        dates.Add(year);
                    }
                }

                // Sort imagedates
                dates.Sort(delegate (ImageDate d1, ImageDate d2) { return d2.id.CompareTo(d1.id); });

                return dates;
            }
        }

        /// <summary>
        /// Return a tree of imagedate
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ImageDate>> GetAllTree()
        {
            // Get flat List
            IEnumerable<ImageDate> flatList = await GetAll();

            // Setup a new list with root albums
            List<ImageDate> treeList = new List<ImageDate>();
            treeList.AddRange(flatList.Where(imagedate => imagedate.pid == 0));

            // Setup parent-child relations
            foreach (ImageDate parentEntry in flatList)
            {
                parentEntry.children = new List<ImageDate>();
                parentEntry.children.AddRange(flatList.Where(imagedate => imagedate.pid == parentEntry.id));
            }

            return treeList;
        }
    }
}
