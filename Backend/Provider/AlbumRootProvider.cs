﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using digiKamWebViewer.Helpers;
using digiKamWebViewer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace digiKamWebViewer.Repository
{
    public interface IAlbumRootProvider
    {
        Task<IEnumerable<AlbumRoot>> GetAll();
        Task<AlbumRoot> GetById(long id);
    }

    public class AlbumRootProvider : IAlbumRootProvider
    {
        private readonly AppSettings appSettings;
        private string baseQuery = "select id, specificPath, identifier from AlbumRoots";

        public AlbumRootProvider(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Return a list of AlbumRoots 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AlbumRoot>> GetAll()
        {
            using var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString());

            return await connection.QueryAsync<AlbumRoot>(baseQuery);
        }

        /// <summary>
        /// Get a specific AlbumRoot
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AlbumRoot> GetById(long id)
        {
            using var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString());

            var enumerator = (await connection.QueryAsync<AlbumRoot>(baseQuery + " where AlbumRoots.id=@albumid", new { albumid = id })).GetEnumerator();
            if (enumerator.MoveNext())
                return enumerator.Current;
            else
                return null;
        }
    }
}
