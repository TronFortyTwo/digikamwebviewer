﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using digiKamWebViewer.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace digiKamWebViewer.Helpers
{
    public interface IUserProvider
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        User GetByUsername(string username);
    }

    public class UserProvider : IUserProvider
    {
        private readonly AppSettings appSettings;
        private readonly PasswordHasher passwordHasher;
        private int ExpiryIn = 604800;  // 7 days in seconds

        public UserProvider(IOptions<AppSettings> appSettings, IOptions<PasswordHasher> passwordHasher)
        {
            this.appSettings = appSettings.Value;
            this.passwordHasher = passwordHasher.Value;
        }

        /// <summary>
        /// Authenticate user
        /// </summary>
        /// <param name="model">Authentication request, with username and password</param>
        /// <returns>Null, if authentication failed. An authentication response, if authentication succeed</returns>
        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            // Search for user with specified username and password
            var user = appSettings.Users.SingleOrDefault(x => x.Username == model.Username && passwordHasher.Check(x.Password, model.Password).Verified);

            // Return null if user not found
            if (user == null) return null;

            // Authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("username", user.Username) }),
                Expires = DateTime.UtcNow.AddSeconds(ExpiryIn),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return new AuthenticateResponse(user, tokenString, ExpiryIn);
        }

        /// <summary>
        /// Get user by username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns></returns>
        public User GetByUsername(string username)
        {
            return appSettings.Users.FirstOrDefault(x => x.Username.Equals(username));
        }
    }
}
