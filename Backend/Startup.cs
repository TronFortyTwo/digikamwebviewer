using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using digiKamWebViewer.Database;
using digiKamWebViewer.Helpers;
using digiKamWebViewer.Repository;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.Extensions.Options;

namespace digiKamWebViewer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(options => {
                options.AddPolicy(MyAllowSpecificOrigins, builder => {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                });
            });
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
            });

            // Add settings
            services.AddTransient<IStartupFilter, SettingValidationStartupFilter>();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddSingleton<IValidatable>(resolver => resolver.GetRequiredService<IOptions<AppSettings>>().Value);

            // Users
            services.AddSingleton<IUserProvider, UserProvider>();
            services.AddSingleton<IPasswordHasher, PasswordHasher>();

            // Database creation handling
            services.AddSingleton<IThumbnailDatabaseBootstrap, ThumbnailDatabase>();

            // Models providers
            services.AddSingleton<IAlbumProvider, AlbumProvider>();
            services.AddSingleton<IAlbumRootProvider, AlbumRootProvider>();
            services.AddSingleton<IImageDateProvider, ImageDateProvider>();
            services.AddSingleton<IImageProvider, ImageProvider>();
            services.AddSingleton<ITagProvider, TagProvider>();
            services.AddSingleton<IThumbnailProvider, ThumbnailProvider>();
            services.AddSingleton<IStatsProvider, StatsProvider>();

            VolumeMapping.GetInstance();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseMiddleware<JwtMiddleware>();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseResponseCompression();
            app.UseEndpoints(endpoints => endpoints.MapControllers());

            serviceProvider.GetService<IThumbnailDatabaseBootstrap>().Setup();
        }
    }
}
