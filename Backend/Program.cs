using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using CommandLine;
using digiKamWebViewer.Helpers;
using System.IO;

namespace digiKamWebViewer
{
    public class CommandLineOptions
    {
        [Option(longName: "generateHash", Required = false, HelpText = "Generate the hash of a password")]
        public string Password { get; set; }

        [Option(longName: "generateRandomString", Default = false, HelpText = "Generate a random string")]
        public bool RandomString { get; set; }

        [Option(longName: "detectDatabasePath", Default = false, HelpText = "Automatically detect the available paths of digikam database")]
        public bool DatabasePath { get; set; }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineOptions>(args)
            .WithParsed(o =>
            {
                // Generate the hash of a password
                if (o.Password!=null)
                {
                    PasswordHasher hasher = new PasswordHasher();
                    Console.WriteLine($"Hash of '{o.Password}': {hasher.Hash(o.Password)}");
                }

                // Generate a random string from a random guid
                if (o.RandomString)
                {
                    string GuidString = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                    Console.WriteLine($"Your random string: {GuidString}");
                }

                // Automatically detect the path of digikam database
                if (o.DatabasePath)
                {
                    // List possible database paths
                    List<String> databasePaths = DatabaseDetector.GetDigikamDatabasePaths();

                    // Print results
                    if (databasePaths.Count > 0)
                    { 
                        Console.WriteLine($"Detected sqlite database paths: ");
                        foreach (string databasePath in databasePaths)
                            Console.WriteLine(databasePath);
                    }else
                        Console.WriteLine("No sqlite database found");
                }
            });

            if(args.Length == 0)
                try { 
                    CreateHostBuilder(args).Build().Run();
                }
                catch(Exception e)
                {
                    Console.WriteLine($"There are some missing or wrong configuration in appsettings.json file: {e.Message}");
                }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseWindowsService()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    #if DEBUG
                        webBuilder.UseWebRoot("../../../../Frontend/dist/spa").UseStartup<Startup>();
                    #else
                        webBuilder.UseWebRoot("spa").UseStartup<Startup>();
                    #endif
                })
            .ConfigureLogging(logging =>
            {
                // Settings for file logging
                logging.AddFile(FileHandling.GetLogFilePath("Log-{Date}.txt"), retainedFileCountLimit: 365, levelOverrides: new Dictionary<string, LogLevel> {
                        { "Microsoft", LogLevel.Warning },
                        { "System", LogLevel.Warning }
                    });

                // Settings for console logging
                logging.AddConsole();
                logging.AddFilter("Microsoft", LogLevel.Warning);
                logging.AddFilter("System", LogLevel.Warning);
            }).ConfigureServices((hostContext, services) => services.AddHostedService<Worker>())
            ;
    }
}
