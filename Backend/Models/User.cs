﻿using System.Text.Json.Serialization;

namespace digiKamWebViewer.Models
{
    /// <summary>
    /// digiKamWebViewer user
    /// </summary>
    public class User
    {
        public string Username { get; set; }
        public string FullName { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
    }
}
