﻿namespace digiKamWebViewer.Models
{
	/// <summary>
	/// In digiKam you can set one or more collections of pictures.
	/// Every collection is an album root.
	/// An album root contains the filesystem identifier, and the relative path of the collection
	/// </summary>
	public class AlbumRoot
	{
		public long id { get; set; }
		public string specificPath { get; set; }
		public string identifier { get; set; }
	}
}
