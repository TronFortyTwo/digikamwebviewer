﻿namespace digiKamWebViewer.Models
{
    /// <summary>
    /// The response returned when the authentication succed
    /// </summary>
    public class AuthenticateResponse
    {
        public User user { get; set; }
        public string Token { get; set; }
        public int ExpiresIn { get; set; }

        public AuthenticateResponse(User user, string token, int expiresIn)
        {
            this.user = user;
            Token = token;
            ExpiresIn = expiresIn;
        }
    }
}
