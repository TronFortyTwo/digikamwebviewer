﻿using System.ComponentModel.DataAnnotations;

namespace digiKamWebViewer.Models
{
    /// <summary>
    /// The body of an authentication request (it has to contain the username and the password)
    /// </summary>
    public class AuthenticateRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
