﻿using System.Collections.Generic;

namespace digiKamWebViewer.Models
{
	/// <summary>
	/// An album of images (aka folder)
	/// </summary>
    public class Album
	{
		public long id { get; set; }
		public long albumRoot { get; set; }
		public string relativePath { get; set; }
		public string name { get; set; }
		public long pid { get; set; }
		public List<Album> children { get; set; }
	}
}
