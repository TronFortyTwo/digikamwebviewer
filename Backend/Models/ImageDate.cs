﻿using System.Collections.Generic;

namespace digiKamWebViewer.Models
{
    /// <summary>
    /// A date node. It could be a year or a month.
    /// </summary>
    public class ImageDate
    {
        public long id { get; set; }
        public long pid { get; set; }
        public long year { get; set; }
        public long month { get; set; }
        public List<ImageDate> children { get; set; }
    }
}
