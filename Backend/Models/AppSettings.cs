using digiKamWebViewer.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace digiKamWebViewer.Helpers
{
    public class AppSettings: IValidatable
    {
        public string Secret { get; set; }
        public string DigiKamDatabaseFolder { get; set; }
        public int ThumbnailMinimumSize { get; set; }
        public int DefaultPictureMinimumSize { get; set; }
        public int VideoSegmentDuration { get; set; }
        public List<User> Users { get; set; }

        public string GetDigiKamDatabaseConnectionString()
        {
            return "Data Source = " + GetDigiKamDatabasePath();
        }

        private string GetDigiKamDatabasePath()
        {
            return Path.Combine(DigiKamDatabaseFolder, "digikam4.db");
        }

        public string GetThumbnailDatabaseConnectionString()
        {
            return "Data Source = " + GetThumbnailDatabasePath();
        }

        private string GetThumbnailDatabasePath()
        {
            return Path.Combine(DigiKamDatabaseFolder, "thumbnails-digikamwebviewer.db");
        }

        /// <summary>
        /// Validate AppSettings
        /// </summary>
        public void Validate()
        {
            if (!File.Exists(GetDigiKamDatabasePath()))
                throw new Exception($"No digiKamDatabase found on {GetDigiKamDatabasePath()}");
            if (Users == null || Users.Count <= 0)
                throw new Exception("No user configured");
            if (DefaultPictureMinimumSize <= 0)
                throw new Exception("No 'DefaultPictureMinimumSize' parameter set");
            if (ThumbnailMinimumSize <= 0)
                throw new Exception("No 'ThumbnailMinimumSize' parameter set");
            if (VideoSegmentDuration <= 0)
                throw new Exception("No 'VideoSegmentDuration' parameter set");
            if (Secret == null || Secret.Length <= 0)
                throw new Exception("No 'Secret' parameter set");
        }
    }
}