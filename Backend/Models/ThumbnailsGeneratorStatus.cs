﻿using System.Collections.Generic;
using System.Threading;
using System.Xml.Linq;

namespace digiKamWebViewer.Models
{
	/// <summary>
	/// The status of the ThumnbailsGenerator work
	/// </summary>
	public class ThumbnailsGeneratorStatus
	{
		public bool isRunning { get; set; }
		public long _processed;
        public long processed
        {
            get { return _processed; }
            set { _processed = value; }
        }
        public long total { get; set; }

        public void Start(int totalCount)
        {
			isRunning = true;
			processed = 0;
			total = totalCount;
        }

		public void Stop()
        {
			isRunning = false;
        }

		public void IncrementCounter()
		{
            Interlocked.Increment(ref _processed);
		}
    }
}
