﻿using System.Collections.Generic;

namespace digiKamWebViewer.Models
{
    /// <summary>
    /// A tag assigned to one or more photo.
    /// Tags could be nested
    /// icon property tells if an icon is assigned to this tag
    /// </summary>
    public class Tag
    {
        public long id { get; set; }
        public long pid { get; set; }
        public string name { get; set; }
        public bool icon { get; set; }
        public List<Tag> children { get; set; }
    }
}
