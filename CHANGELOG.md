## [3.1.1] - 2023-10-08

* Improved performance in thumbnail bulk creation
* Create thumbnail for audio file

## [3.1.0] - 2023-10-02

* Audio are now encoded and streamed in realtime from the backend
* Updated .NET framework

## [3.0.0] - 2023-09-21

* Video are now encoded and streamed in realtime from the backend
* Added left/right keypress on Slideshow view
* It's possible to detect the digiKam database path with the CLI command --detectDatabasePath

## [2.0.2] - 2022-05-08

* Images are now sorted by digitalization date

## [2.0.1] - 2022-05-06

* Fixed an error while generating cropped tags thumbnails

## [2.0.0] - 2022-03-27

* Changed approach in thumbnail generation for better quality: the smaller side will be equal to the size indicated
* Bulk thumbnails generation from settings

## [1.2.0] - 2022-03-13

* Added map view
* Performance update on slideshow view
* Minor UI improvements

## [1.1.1] - 2021-12-19

* Fixed compatibility with digiKam 7.4
* Imagedate list endpoint is now sorted by date desc
* Fix month parsing on Grid view

## [1.1.0] - 2021-11-25

* Added creation of thumbnails for videos
* Added toolbar buttons tooltip
* Logins are now logged

## [1.0.1] - 2021-11-15

* Fixed bulk download

## [1.0.0] - 2021-11-14

* First release
