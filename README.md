# digiKamWebViewer

☁ Are you paranoid about your privacy and cloud storage? 🤨\
💸 Or simply, you don't want to have a fixed cost to store your 3,14 TB of photo collection? 😣\
🔍 Did you search for other free self-hosted galleries but you found them too heavy or ugly? 🤮\
💻 And you are skilled enough to have your personal webserver? 🤓\
So, *digiKamWebViewer* it's what you were searching for!

## Key features

*digiKamWebViewer* is a self-hosted open source web photo gallery which use the power of [digiKam](https://www.digikam.org/) to handle your photos 📸\
It uses the digiKam database to index and locate your pictures. It collects all digiKam metadata to show your photos in a simple and light view.

With *digiKamWebViewer* you can browse your pictures by:

* 📁 *Albums*: view your photos in a classic folder view
* 🏷️ *Tags*: search for photos with a specific tags
* 📅 *Dates*: show your photos within a timeline
* 🗺️ *Map*: search your photos within a map

📸 To ensure high-performance photo viewing, photos are resized in real time by the backend.\
📹 To ensure high-performance viewing of videos, even large or uncompressed ones, they are recoded in real time by the backend (for this feature [ffmpeg](https://ffmpeg.org/) and adequate performance of the backend are needed).\
⬇️ However, if you want the data in the original format, you can download individual photos, albums or tags.

digiKamWebViewer is meant for personal use and to view photos from your home computer/server (maybe behind a VPN). So no photo upload, edit or sharing are planned in the near future. 

## Technical details

### Backend
The backend is build with .net core. \
It reads digiKam sqlite database and it exposes data throgh a RESTful web service.
It can run on any platform (Windows/Linux/x86-64/arm). \
You can find backend [API documentation here](/Backend/README.md).

### Frontend
The frontend is written in [Vue3](https://v3.vuejs.org) and it uses [Quasar framework](https://quasar.dev). \
It's optimized to be used with mobile devices.

## Configuration

You have to create the *appsettings.json* configuration file. You can create a copy of *appsettings.template.json* file and then setup the following parameters:

* _AuthSecret_: client authentication is based on this secret key. Just put a random string. If you are lazy you can generate it by running: `digiKamWebViewer --generateRandomString`
* _YourPasswordHash_: your user password hash. You can generate it by running: `digiKamWebViewer --generateHash YourPassword`
* _digiKamDatabaseFolder_: the folder containing the digiKam database. You can search for it by running:  `digiKamWebViewer --detectDatabasePath`

```
"AppSettings":{
	"Secret": "{AuthSecret}",
	"Users": [
		{
			"Username": "User",
			"FullName": "User",
			"Password": "{YourPasswordHash}"
		}
	],
	"ThumbnailMinimumSize": 200,
	"DefaultPictureMinimumSize": 1600,
    "VideoSegmentDuration": 4,
	"DigiKamDatabaseFolder": "{digiKamDatabaseFolder}"
  }
```

Optional: If you want _digiKamWebViewer_ to create previews for videos and to stream videos, you need to install [ffmpeg](https://ffmpeg.org/).

## Screenshots

### Album view
![Album selection](Docs/screenshot_album_1.png "Album selection") ![Grid view](Docs/screenshot_album_2.png "Grid view") ![Slideshow](Docs/screenshot_album_3.png "Slideshow")

### Tags view
![Tag selection](Docs/screenshot_tag_1.png "Tag selection") ![Grid view](Docs/screenshot_tag_2.png "Grid view") ![Slideshow](Docs/screenshot_tag_3.png "Slideshow")

### Map view
![Map view](Docs/screenshot_map_1.png "Map view") ![Hover popup](Docs/screenshot_map_2.png "Hover popup") ![Slideshow](Docs/screenshot_map_3.png "Slideshow")
