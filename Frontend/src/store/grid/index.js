export default {
    namespaced: true,
    state: function () {
      return {
        data: [],
        scrollY: 0
      }
    },
    mutations: {
      updateData (state, value) {
        state.data = value
      },
      updateScrollY (state, value) {
        state.scrollY = value
      }
    }
  }
  