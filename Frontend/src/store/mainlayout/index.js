export default {
  namespaced: true,
  state: function () {
    return {
      leftDrawerOpened: false
    }
  },
  mutations: {
    updateLeftDrawerOpened (state, value) {
      state.leftDrawerOpened = value
    }
  }
}
