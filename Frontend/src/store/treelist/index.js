export default {
    namespaced: true,
    state: function () {
      return {
        filter: "",
        data: [],
        expandedKeys: [],
        ticked: [],
        scrollY: 0
      }
    },
    mutations: {
      updateFilter (state, value) {
        state.filter = value
      },
      updateData (state, value) {
        state.data = value
      },
      updateExpandedKeys (state, value) {
        state.expandedKeys = value
      },
      updateScrollY (state, value) {
        state.scrollY = value
      },
      updateTicked (state, value) {
        state.ticked = value
      }
    }
  }
  