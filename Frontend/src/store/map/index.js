export default {
    namespaced: true,
    state: function () {
      return {
        data: [],
        mapSettings: []
      }
    },
    mutations: {
      updateData (state, value) {
        state.data = value
      },
      updateMapSettings (state, value) {
        state.mapSettings = value
      }
    }
  }
  