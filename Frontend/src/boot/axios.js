import { boot } from 'quasar/wrappers'
import axios from 'axios'


var url;
if (!process.env.DEV) { 
  url = '/api'
}else{ 
  url = 'http://'+window.location.hostname+':5001/api' // DEBUG api url 
}

const api = axios.create({ 
  baseURL: url
})

// Add JWT token to the requests
api.interceptors.request.use(function (config) {
  config.headers.common['Authorization'] = "Bearer " + localStorage.getItem("token");
  return config;
}, null, { synchronous: true });

// If an api call returns 401 unauthorized, go to the login page
api.interceptors.response.use(null, (error) => {
  if (error.response.status == 401)
    window.location.href = "#/Login";
  return Promise.reject(error.message);
});

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

export { axios, api }