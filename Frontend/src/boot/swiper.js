import { boot } from 'quasar/wrappers'
import SwiperCore, { Navigation, Pagination, Zoom, Lazy } from 'swiper/core';

// import style (>= Swiper 6.x)
import 'swiper/swiper-bundle.css'
import "swiper/components/zoom/zoom.min.css"
import "swiper/components/lazy/lazy.min.css"
import "swiper/components/navigation/navigation.min.css"
import "swiper/components/pagination/pagination.min.css"
// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async (/* { app, router, ... } */) => {
  // something to do
  SwiperCore.use([Zoom,Lazy,Navigation,Pagination]);
})
