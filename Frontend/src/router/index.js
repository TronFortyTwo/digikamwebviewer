import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const Router = createRouter({
    // Do not reset scroll position after routing
    // scrollBehavior: () => ({ left: 0, top: 0 }),
    routes: [
      {
        path: '/',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: '', component: () => import('pages/Index.vue') }
        ]
      },
      {
        path: '/Albums',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: '', component: () => import('pages/Albums.vue'), meta: { title: "Albums" } }
        ]
      },
      {
        path: '/Tags',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: '', component: () => import('pages/Tags.vue'), meta: { title: "Tags" } }
        ]
      },
      {
        path: '/Dates',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: '', component: () => import('pages/Dates.vue'), meta: { title: "Dates" } }
        ]
      },
      {
        path: '/Grid',
        component: () => import('layouts/BackLayout.vue'),
        children: [
          { path: '', component: () => import('pages/Grid.vue') }
        ]
      },
      {
        path: '/Slideshow',
        component: () => import('layouts/BackLayout.vue'),
        children: [
          { path: '', component: () => import('src/pages/Slideshow.vue') }
        ]
      },
      {
        path: '/Map',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: '', component: () => import('src/pages/Map.vue'), meta: { title: "Map" } }
        ]
      },
      {
        path: '/MapOf',
        component: () => import('layouts/BackLayout.vue'),
        children: [
          { path: '', component: () => import('src/pages/Map.vue'), meta: { title: "Map" } }
        ]
      },
      {
        path: '/System',
        component: () => import('layouts/MainLayout.vue'),
        children: [
          { path: '', component: () => import('src/pages/System.vue'), meta: { title: "System" } }
        ]
      },
      {
        path: '/Login',
        component: () => import('layouts/PageLayout.vue'),
        children: [
          { path: '', component: () => import('src/pages/Login.vue') }
        ]
      },

      // Always leave this as last one,
      // but you can also remove it
      {
        path: '/:catchAll(.*)*',
        component: () => import('pages/Error404.vue')
      }
    ],

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE)
  })

  Router.beforeEach((to, from, next) => {
    var storedToken = localStorage.getItem("token")
    var tokenExpiryTime = localStorage.getItem("tokenExpiryTime")
    var isAuthenticated = (tokenExpiryTime != null && tokenExpiryTime > new Date().valueOf()/1000) && (storedToken != null && storedToken.length > 0)
    if (to.path !== "/Login" && !isAuthenticated) {
      if (to.path == '/')
        next('/Login')
      else
        Router.push({
          path: "/Login",
          query: {
            redirect: to.path,
          }
        })
    } else
      next()
  })

  return Router
})
